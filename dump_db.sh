#!/bin/sh    
hostname=`hostname`

##########################################
## OpenERP Backup
## Backup databases: openerpdb1, openerpdb2
##########################################

# Stop OpenERP Server
/etc/init.d/openerp-server stop

# Dump DBs
for db in openerpdb1 openerpdb2
do
  date=`date +"%Y%m%d_%H%M%N"`
  filename="/var/pgdump/${hostname}_${db}_${date}.sql"
  pg_dump -E UTF-8 -p 5433 -F p -b -f $filename $db
  gzip $filename
done

# Start OpenERP Server
/etc/init.d/openerp-server start

exit 0
