Basic Backup Script
===================


- Login as the root user ::

  > sudo su -i 
 

- Download dump.sh_ and place it in the  /var/scripts directory if the /var/scripts directory does not exists create it ::

  > mkdir /var/scripts
  > cd /var/scripts
  > wget https://bitbucket.org/zaxklone/openerp_backup/raw/d741956f780fcde3dd38535e40aa69c5526c2376/dump_db.sh


- Download  housekeeping.sh_ and place it in the /var/scripts directory ::

  > cd /var/scripts
  > wget https://bitbucket.org/zaxklone/openerp_backup/raw/d741956f780fcde3dd38535e40aa69c5526c2376/housekeeping.sh

- Scheduling the backups by editing the /etc/crontab to run daily at 1am ::

  # m h dom mon dow user command
  # 0 1 * * * postgres /var/scripts/dump_db.sh
  # 0 5 * * * postgres /var/scripts/housekeeping.sh








.. __housekeeping.sh : https://bitbucket.org/zaxklone/openerp_backup/raw/d741956f780fcde3dd38535e40aa69c5526c2376/housekeeping.sh
__ housekeeping.sh_

.. __dump.sh : https://bitbucket.org/zaxklone/openerp_backup/raw/d741956f780fcde3dd38535e40aa69c5526c2376/dump_db.sh
__ dump.sh_
